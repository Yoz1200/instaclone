class PasswordEditController < ApplicationController
  def edit
    @user = User.find(params[:id])
  end
  
  def update
    @user = User.find(params[:id])
    if @user.authenticate(params[:user][:now_password])
      if params[:user][:password].empty?
        @user.errors.add(:password, :blank)
        render 'edit'
      elsif @user.update_attributes(user_params)
        log_in @user
        flash[:success] = "Password has been changed."
        redirect_to @user
      else
        render 'edit'
      end
    else
      @user.errors.add(:now_password, ": now password is wrong")
      #flash.now[:danger] = "now password is wrong"
      render 'edit'
    end
  end
  
  private

  def user_params
    params.require(:user).permit(:password, :password_confirmation)
  end
end
