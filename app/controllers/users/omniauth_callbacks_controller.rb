class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController

  # Facebookを利用したログイン時に起動
  def facebook
    callback_from :facebook
  end
 
  private
 
    # Facebook連携で取得した情報でログイン
    def callback_from(provider)
      provider = provider.to_s
  
      @user = User.find_for_oauth(request.env['omniauth.auth'])
      if @user.persisted?
        flash[:notice] = I18n.t('devise.omniauth_callbacks.success', kind: provider.capitalize)
        log_in @user
        redirect_back_or @user
      else
        session["devise.#{provider}_data"] = request.env['omniauth.auth']
        redirect_to root_url
      end
    end
end