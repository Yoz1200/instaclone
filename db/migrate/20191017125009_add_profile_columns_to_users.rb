class AddProfileColumnsToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :user_name, :string
    add_column :users, :website, :string
    add_column :users, :self_introduction, :string
    add_column :users, :phone_number, :string
    add_column :users, :sex, :integer
  end
end
