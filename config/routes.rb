Rails.application.routes.draw do
  
  devise_for :users, skip: [:sessions],
    controllers: {
      omniauth_callbacks: 'users/omniauth_callbacks'
    }

  root 'static_pages#home'
  get  '/about',     to: 'static_pages#about'
  get  '/signup',    to: 'users#new'
  post '/signup',    to: 'users#create'
  get  '/login',     to: 'sessions#new'
  post '/login',     to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'
  resources :users do
    member do
      get :following, :followers
    end
  end
  resources :account_activations, only: [:edit]
  resources :microposts,          only: [:create, :destroy]
  resources :relationships,       only: [:create, :destroy]
  resources :password_edit,       only: [:edit, :update]
end
